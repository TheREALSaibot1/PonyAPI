const fs = require('fs')
const path = require('path')
const _ = require('lodash')
const Koa = require('koa')
const mount = require('koa-mount')
const bodyParser = require('koa-bodyparser')
const config = require('config')

const ponych = require('./api/pony-challenge/pony-challenge')

const mongoose = require('./lib/mongoose')
const error = require('./lib/error')

// Some variables used in app

const app = new Koa()
let server = {}

function connectDB(connection = undefined) {
	const conn = connection || config.db.connection

	return mongoose.connect(conn, {
		useMongoClient: true
	})
}

function setup() {
	// app.context.db = mongoose

	app.use(bodyParser())

	app.use(error)

	app.use(mount(ponych))

	return app
}

async function start(port = undefined, mongo = undefined) {
	try {
		await connectDB(mongo)
	} catch (err) {
		console.log('Could not connect to database!')
		return false
	}

	setup()

	const theport = port || config.port

	server = app.listen(theport)

	console.log('App started on port:', theport)

	return server
}

module.exports = {
	start,
	app,
	server
}
