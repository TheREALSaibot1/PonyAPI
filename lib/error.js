const error = require('koa-json-error')
const compose = require('koa-compose')
const _ = require('lodash')

const options = {
	// Avoid showing the stacktrace in 'production' env
	postFormat: (e, obj) => process.env.NODE_ENV === 'production' ? _.omit(obj, 'stack') : obj
}

module.exports = compose([error(options), async (ctx, next) => {
	try {
		await next()
	} catch (err) {

		ctx.throw(err)
	}
}])
