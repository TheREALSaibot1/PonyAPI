const Koa = require('koa')
const router = require('koa-joi-router')
const mount = require('koa-mount')

const maze = require('./maze/maze')

const app = new Koa()

app.use(mount('/pony-challenge', maze))

module.exports = app
