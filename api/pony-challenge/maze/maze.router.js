const router = require('koa-joi-router')

const maze = require('./maze.methods')

const mazeRouter = router()

const routes = [{
	method: 'get',
	path: '/test',
	handler: maze.test
}, {
	method: 'post',
	path: '/',
	handler: maze.create
}, {
	method: 'get',
	path: '/:mazeId',
	handler: maze.state
}, {
	method: 'post',
	path: '/:mazeId',
	handler: maze.move
}, {
	method: 'get',
	path: '/:mazeId/print',
	handler: maze.print
}]

mazeRouter.prefix('/maze')

mazeRouter.route(routes)

module.exports = mazeRouter.middleware()
