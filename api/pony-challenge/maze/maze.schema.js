const mongoose = require('mongoose')

const Schema = mongoose.Schema

const MazeSchema = new Schema({
	pony: Number,
	exit: Number,
	domokun: Number,
	size: {
		type: [{
			type: Number,
			min: 15,
			max: 20
		}],
		validate: [arrayLimit, '{PATH} needs 2 values, width and height']
	},
	difficulty: {
		type: Number,
		min: 1,
		max: 15
	},
	data: [{
		type: Array,
		enum: ['west', 'north']
	}],
	gameState: {
		state: {
			type: String,
			enum: ['active', 'completed'],
			default: 'active'
		},
		stateResult: String
	},
}, {
	versionKey: false
})

function arrayLimit(val) {
	return val.length === 2
}

module.exports = MazeSchema
