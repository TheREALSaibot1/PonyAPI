const Koa = require('koa')

const maze = require('./maze.router.js')

const app = new Koa()

app.use(maze)

module.exports = app
