const mongoose = require('mongoose')

const MazeSchema = require('./maze.schema')

module.exports = mongoose.model('Maze', MazeSchema)
