const uniqueRandom = require('unique-random')

const Maze = require('./maze.model')

const test = async ctx => {
	ctx.body = "Yay it sparkles!"
}

const create = async ctx => {
	if (!ctx.request.body['maze-width'] ||
		!ctx.request.body['maze-height'] ||
		!ctx.request.body['maze-player-name'] ||
		!ctx.request.body['difficulty']) {
		ctx.throw(400, 'missing key in body')
	}

	const body = ctx.request.body
	const mazeWidth = body['maze-width']
	const mazeHeight = body['maze-height']
	const difficulty = body['difficulty']

	const mazeLength = mazeWidth * mazeHeight
	let maze = []

	for (let i = 0; i < mazeLength; i++) {
		let newField = []

		if (i === 0 || i % mazeWidth === 0) {
			newField.push('west')
		} else if (!newField.find(f => {
			return f === 'west'
		})) {
			const random = Math.round(Math.random())
			if (random === 0) {
				newField.push('west')
			}
		}

		if (i < mazeWidth) {
			newField.push('north')
		} else if (!newField.find(f => {
			return f === 'north'
		})) {
			const random = Math.round(Math.random())
			if (random === 0) {
				newField.push('north')
			}
		}

		maze.push(newField)
	}

	const mazeFields = uniqueRandom(0, mazeLength - 1)
	const pony = mazeFields()
	const exit = mazeFields()
	const domokun = mazeFields()

	const gameState = {
		state: 'active',
		stateResult: 'Successfully created'
	}

	const newMaze = await Maze.create({
		pony,
		exit,
		domokun,
		size: [
			mazeWidth,
			mazeHeight
		],
		difficulty,
		data: maze,
		gameState
	})

	ctx.body = {
		mazeId: newMaze._id
	}
}

const state = async ctx => {
	const params = ctx.params
	const id = params.mazeId

	ctx.body = await Maze.findById(id)
}

const move = async ctx => {
	const body = ctx.request.body

	if (!body.direction) {
		ctx.throw(400, 'Direction is needed')
	}

	const direction = body.direction
	const possibleDirections = ['north', 'east', 'south', 'west']

	if (!possibleDirections.find(d => {
		return direction === d
	})) {
		ctx.throw(400, 'Possible directions east, west, north, south')
	}

	const id = ctx.params.mazeId
	let maze = await Maze.findById(id)
	let result = {}

	if (direction === 'north') {
		if (maze.data[maze.pony].find(d => {
			return d === 'north'
		})) {
			result = {
				state: 'active',
				stateResult: "Can't walk in there"
			}
		} else {
			result = {
				state: 'active',
				stateResult: "Move accepted"
			}

			maze.pony -= maze.size[0]
		}
	}

	if (direction === 'east') {
		if (maze.data[maze.pony + 1].find(d => {
			return d === 'west'
		})) {
			result = {
				state: 'active',
				stateResult: "Can't walk in there"
			}
		} else {
			result = {
				state: 'active',
				stateResult: "Move accepted"
			}

			maze.pony += 1
		}
	}

	if (direction === 'south') {
		const southField = maze.pony + maze.size[0]
		if (maze.data[southField].find(d => {
			return d === 'north'
		})) {
			result = {
				state: 'active',
				stateResult: "Can't walk in there"
			}
		} else {
			result = {
				state: 'active',
				stateResult: "Move accepted"
			}

			maze.pony += maze.size[0]
		}
	}

	if (direction === 'west') {
		if (maze.data[maze.pony].find(d => {
			return d === 'west'
		})) {
			result = {
				state: 'active',
				stateResult: "Can't walk in there"
			}
		} else {
			result = {
				state: 'active',
				stateResult: "Move accepted"
			}

			maze.pony -= 1
		}
	}

	await maze.save()

	ctx.body = result
}

const print = async ctx => {
	const id = ctx.params.mazeId

	const maze = await Maze.findById(id)
	const data = maze.data

	let mazeParts = []

	while (data.length) {
		mazeParts.push(data.splice(0, maze.size[0]))
	}

	let printedMap = ''

	// console.log(mazeParts)

	mazeParts.forEach((p, index) => {
		p.forEach((row, rindex) => {
			let lineLeft = 2

			for (let i = 0; i < lineLeft; i++) {

				// Ceiling

				if (i === 0 && row.includes('north')) {
					printedMap += ' ---'
				} else if (i === 0 && !row.includes('north')) {
					printedMap += '    '
				}

				if (i === 0 && rindex === (maze.size[0])) {
					printedMap += '\r\n'
					// console.log('added linebreak')
				}

				// Mazetile

				if (i === 1 && row.includes('west')) {
					printedMap += '| '
				} else if (i === 1 && !row.includes('west')) {
					printedMap += '  '
				}

				let indexInMaze = (maze.size[0]*index) + rindex

				if (i === 1 && indexInMaze === maze.pony) {
					printedMap += 'P '
				} else if (i === 1 && indexInMaze === maze.exit) {
					printedMap += 'E '
				} else if (i === 1 && indexInMaze === maze.domokun) {
					printedMap += 'D '
				} else {
					printedMap += '  '
				}

				if (i === 1 && rindex === (maze.size[0]-1)) {
					printedMap += '|\r\n'
					// console.log('added linebreak')
				}
			}
		})
	})

	for (let i = 0; i < maze.size[0]; i++) {
		printedMap += ' ---'
	}

	console.log(printedMap)

	ctx.body = printedMap
}

module.exports = {
	test,
	create,
	state,
	move,
	print
}
